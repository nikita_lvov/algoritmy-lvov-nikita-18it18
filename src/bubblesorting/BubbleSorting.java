package bubblesorting;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Класс сортировки массива и ArrayList пузырьковым способом
 *
 * @autor Nikita Lvov 18it18
 */
public class BubbleSorting {
    public static void main(String[] args) {
        int[] array = {4, 5, 1, 3, 7, 9, 8, 6, 2};

        System.out.println("Исходный массив: ");
        System.out.println(Arrays.toString(array));

        bubbleSort(array);
        System.out.println("Отсортированный массив: ");
        System.out.println(Arrays.toString(array));

        ArrayList<Integer> arrayList = new ArrayList<>(Arrays.asList(4, 5, 1, 3, 7, 9, 8, 6, 2));

        System.out.println("Исходный ArrayList: ");
        System.out.println(arrayList);

        bubbleSort(arrayList);
        System.out.println("Отсортированный ArrayList: ");
        System.out.println(arrayList);
    }

    /**
     * Метод сортирующий пузырьковым способом массив
     *
     * @param array массив целых чисел
     */
    private static void bubbleSort(int[] array) {
        int replace;
        boolean isSorted = false;
        while (!isSorted) {
            isSorted = true;
            for (int i = 0; i < array.length - 1; i++) {
                if (array[i] > array[i + 1]) {
                    isSorted = false;

                    replace = array[i];
                    array[i] = array[i + 1];
                    array[i + 1] = replace;
                }
            }
        }
    }

    /**
     * Метод сортирующий пузырьковым способом ArrayList
     *
     * @param arrayList строчный массив целых чисел
     */
    private static void bubbleSort(ArrayList<Integer> arrayList) {
        int replace;
        boolean isSorted = false;
        while (!isSorted) {
            isSorted = true;
            for (int i = 0; i < arrayList.size() - 1; i++) {
                if (arrayList.get(i) > arrayList.get(i + 1)) {
                    isSorted = false;

                    replace = arrayList.get(i);
                    arrayList.set(i, arrayList.get(i + 1));
                    arrayList.set((i + 1), replace);
                }
            }
        }
    }
}

