package person;

import java.util.Scanner;

/**
 * Класс реализующий
 * поиск по фамилиям
 * сортировку по году рождения
 * сортировку по фамилиям
 *
 * @author Nikita Lvov 18it18
 */
public class Main {
    public static void main(String[] args) {
        Person person1 = new Person("Львов", 2002);
        Person person2 = new Person("Иванова", 2005);
        Person person3 = new Person("Александров", 2009);
        Person person4 = new Person("Лионов", 1992);

        Person[] persons = {person1, person2, person3, person4};

        Person[] surnamePersons = new Person[persons.length];

        System.out.println("Сортировка по году рождения - ");
        bubbleSort(persons);
        print(persons);
        System.out.println("Сортировка по фамилиям - ");
        surnameSort(persons);
        print(persons);
        searchSurname(persons, surnamePersons);
        println(surnamePersons);
    }

    /**
     * Метод сортируюзщий персоны по году рождения ( по возрастанию )
     *
     * @param persons массив персон
     */
    private static void bubbleSort(Person[] persons) {
        Person replace;
        boolean isSorted = false;
        while (!isSorted) {
            isSorted = true;
            for (int i = 0; i < persons.length - 1; i++) {
                if (persons[i].getYear() > persons[i + 1].getYear()) {
                    isSorted = false;

                    replace = persons[i];
                    persons[i] = persons[i + 1];
                    persons[i + 1] = replace;
                }
            }
        }
    }

    /**
     * Метод сортирующий фамилии по алфавиту
     *
     * @param persons массив персон
     */
    private static void surnameSort(Person[] persons) {
        for (int i = 1; i < persons.length; i++) {
            Person person1 = persons[i];
            int j = i;
            while (j > 0 && persons[j - 1].getSurname().compareTo(person1.getSurname()) >= 0) {
                persons[j] = persons[j - 1];
                j--;
            }
            persons[j] = person1;
        }
    }

    /**
     * Метод поиска фамилий в массиве
     *
     * @param persons        массив персон
     * @param surnamePersons массив фамилий
     */
    private static void searchSurname(Person[] persons, Person[] surnamePersons) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Введите фамилию");
        String surname = sc.nextLine();
        for (int i = 0; i < persons.length; i++) {
            if (surname.equals(persons[i].getSurname())) {
                surnamePersons[i] = persons[i];
            }
        }
    }

    /**
     * Метод вывода
     *
     * @param persons массив персон
     */
    private static void print(Person[] persons) {
        for (Person person : persons) {
            System.out.println(person);
        }
        System.out.println();
    }

    /**
     * Метод выводящий фамилии , которые есть в списке
     *
     * @param surnamePersons список фамилий
     */
    private static void println(Person[] surnamePersons) {
        if (surnamePersons.length == 0) {
            System.out.println("Такие фамилий нет в списке");
        } else {
            for (Person surnamePerson : surnamePersons) {
                System.out.println(surnamePerson + " ");
            }
        }
    }
}
