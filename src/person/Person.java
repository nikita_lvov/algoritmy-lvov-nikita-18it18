package person;

/**
 * Класс персон
 *
 * @author Nikita Lvov 18it18
 */
public class Person {
    private String surname;
    private int year;

    Person(String surname, int year) {
        this.surname = surname;
        this.year = year;
    }

    String getSurname() {
        return surname;
    }

    int getYear() {
        return year;
    }

    @Override
    public String toString() {
        return "Человек: " +
                "Фамилия=" + surname +
                ", Год рождения=" + year + "}\n";
    }
}
