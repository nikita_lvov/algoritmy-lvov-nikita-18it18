package searchalgorithm;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;

/**
 * Класс для последовательного поиска элемента в массиве и ArrayList
 *
 * @author Nikita Lvov 18it18
 */
public class SearchAlgorithm {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int[] mas = {1, 2, 3, 4, 5};
        System.out.println("Массив: " + Arrays.toString(mas));
        System.out.println("Введите число для проверки: ");
        int number = sc.nextInt();
        int indexMas = searchIndexInMas(mas, number);
        if (indexMas == -1) {
            System.out.println("Нет такого элемента");
        } else {
            System.out.println("Индекс числа - " + indexMas + " - это число есть в массиве ");
        }
        ArrayList<Integer> arrayList = new ArrayList<>(Arrays.asList(1, 2, 3, 4, 5));
        System.out.println("ArrayList: " + arrayList);
        int indexArrayList = searchIndexInArrayList(arrayList, number);
        if (indexArrayList == -1) {
            System.out.println("Нет такого элемента");
        } else {
            System.out.println("\n Индекс числа - " + indexArrayList + " - это число есть в ArrayList ");
        }
    }

    /**
     * Метод поиска элемента в массиве
     *
     * @param mas    массив
     * @param number значение для поиска
     * @return индекс числа если оно имеется в массиве; -1 ,если если его нет
     */
    private static int searchIndexInMas(int[] mas, int number) {
        for (int i = 0; i < mas.length; i++) {
            if (mas[i] == number) {
                return i;
            }
        }
        return -1;
    }

    /**
     * Метод поиска элемента в ArrayList
     *
     * @param arrayList строчный массив
     * @param number    значение для поиска
     * @return индекс числа если оно имеется в ArrayList; -1 ,если если его нет
     */
    private static int searchIndexInArrayList(ArrayList<Integer> arrayList, int number) {
        for (int i = 0; i < arrayList.size(); i++) {
            if (arrayList.get(i) == number)
                return i;
        }
        return -1;
    }
}
