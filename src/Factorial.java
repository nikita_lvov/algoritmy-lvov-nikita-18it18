import java.util.Scanner;

/**
 * Класс нахождения факториала
 *
 * @autor Lvov Nikita 18it18
 */
public class Factorial {
    private static Scanner sc = new Scanner(System.in);

    public static void main(String[] args) {
        System.out.println("Введите число, факториал которого хотите вычислить (положительное)");
        int n = sc.nextInt();
        if (n < 0) {
            System.out.println("Факториал должен быть положительным числом");
            return;
        }
        System.out.println("Результат: " + searchFactorial(n));
        System.out.println("Результат рекурсивного способа: " + factorial(n));
    }

    /**
     * Метод нахождения факториала
     *
     * @param n неотрицательное число
     * @return факториал
     */
    private static long searchFactorial(int n) {
        long factorial = 1;
        for (int i = 1; i <= n; i++) {
            factorial = factorial * i;
        }
        return factorial;
    }

    /**
     * Метод нахождения факториала рекурсивным способом
     *
     * @param n неотрицательное число
     * @return факториал
     */
    private static long factorial(int n) {
        if (n == 0 || n == 1) {
            return 1;
        }
        return factorial(n - 1) * n;
    }
}
