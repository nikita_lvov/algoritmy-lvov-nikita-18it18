import java.util.Scanner;

/**
 * Теорема Пифагора
 *
 * @author Nikita Lvov 18it18
 */
public class С {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n, x, y, z;
        System.out.println("Введите n: ");
        n = sc.nextInt();
        x = 1;
        while (x < n) {
            y = x;
            while (y <= n) {
                z = y;
                while (z <= n) {
                    if ((z <= x * 2) && (y <= x * 2) && (z * z == x * x + y * y)) {
                        System.out.println(x + " " + y + " " + z);
                        break;
                    }
                    z++;
                }
                y++;
            }
            x++;
        }
    }
}

