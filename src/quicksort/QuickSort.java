package quicksort;

import java.util.Arrays;

public class QuickSort {
    public static void main(String[] args) {
        int[] array = {10, 5, 2, 3};

        System.out.println("Исходный массив");
        System.out.println(Arrays.toString(array));

        int less = 0;
        int greater = array.length - 1;
        qSort(array,less,greater);

        System.out.println("Отсортированный массив");
        System.out.println(Arrays.toString(array));
    }

    /**
     * Метод быстрой сортировки массива
     *
     * @param array массив целых чисел
     * @param less подмассив чисел меньше опорного числа
     * @param greater подмассив чисел больше опорного числа
     */
    private static void qSort(int[] array, int less, int greater) {
        if (array.length < 2) {
            return; //завершить выполнение
        } else {
            // выбрать опорный элемент
            int middle = less + (greater - less) / 2;
            int pivot = array[middle];

            // разделить на подмассивы, который больше и меньше опорного элемента
            int i = less, j = greater;
            while (i <= j) {
                while (array[i] < pivot) {
                    i++;
                }

                while (array[j] > pivot) {
                    j--;
                }

                if (i <= j) {//меняем местами
                    int temp = array[i];
                    array[i] = array[j];
                    array[j] = temp;
                    i++;
                    j--;
                }
            }

            // вызов рекурсии для сортировки левой и правой части
            if (less < j)
                qSort(array, less, j);

            if (greater > i)
                qSort(array, i, greater);
        }
    }
}
