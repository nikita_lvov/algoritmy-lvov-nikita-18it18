package binarysearch;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;

/**
 * Класс для бинарного поиска элемента в массиве и ArrayList
 *
 * @author Nikita Lvov 18it18
 */
public class BinarySearch {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int[] mas = {1, 2, 3, 4, 5};// отсортированный массив
        ArrayList<Integer> arrayList = new ArrayList<>(Arrays.asList(1, 2, 3, 4, 5));// отсортированный ArrayList
        System.out.println("Массив: " + Arrays.toString(mas));
        System.out.println("ArrayList: " + arrayList);
        System.out.println("Введите число для проверки: ");
        int number = sc.nextInt();

        int indexMas = binarySearch(mas, number);
        if (indexMas == -1) {
            System.out.println("Нет такого элемента");
        } else {
            System.out.println("Индекс числа - " + indexMas + " - это число есть в массиве ");
        }

        int indexArrayList = binarySearch(arrayList, number);
        if (indexArrayList == -1) {
            System.out.println("Нет такого элемента");
        } else {
            System.out.println("\n Индекс числа - " + indexArrayList + " - это число есть в ArrayList ");
        }
    }

    /**
     * Возвращает индекс элемента, если число {@code number}
     * найдено в массиве {@code mas}
     * или -1, если искомого числа в массиве нет
     *
     * @param mas    отсортированный массив целых чисел
     * @param number искомое целое число
     * @return индекс элемента массива, если число найдено
     * или -1 в обратном случае
     */
    private static int binarySearch(int[] mas, int number) {
        int first = 0;
        int last = mas.length - 1;
        while (first <= last) {
            int mid = (last + first) / 2;
            int guess = mas[mid];
            if (guess == number) {
                return mid;
            }
            if (guess < number) {
                first = mid + 1;
            } else {
                last = mid - 1;
            }
        }
        return -1;
    }

    /**
     * Возвращает индекс элемента, если число {@code number}
     * найдено в ArrayList {@code arrayList}
     * или -1, если искомого числа в ArrayList нет
     *
     * @param arrayList отсортированный ArrayList целых чисел
     * @param number    искомое целое число
     * @return индекс элемента ArrayList, если число найдено
     * или -1 в обратном случае
     */
    private static int binarySearch(ArrayList<Integer> arrayList, int number) {
        int first = 0;
        int last = arrayList.size() - 1;
        while (first <= last) {
            int mid = (last + first) / 2;
            int guess = arrayList.get(mid);
            if (guess == number) {
                return mid;
            }
            if (guess < number) {
                first = mid + 1;
            } else {
                last = mid - 1;
            }
        }
        return -1;
    }
}


