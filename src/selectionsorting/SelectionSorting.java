package selectionsorting;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Класс сортировки массива и ArrayList способом выбора
 *
 * @autor Nikita Lvov 18it18
 */
public class SelectionSorting {
    public static void main(String[] args) {
        int[] array = {4, 5, 1, 3, 7, 9, 8, 6, 2};

        System.out.println("Исходный массив: ");
        System.out.println(Arrays.toString(array));

        selectionSort(array);
        System.out.println("Отсортированный массив: ");
        System.out.println(Arrays.toString(array));

        ArrayList<Integer> arrayList = new ArrayList<>(Arrays.asList(4, 5, 1, 3, 7, 9, 8, 6, 2));

        System.out.println("Исходный ArrayList: ");
        System.out.println(arrayList);

        selectionSort(arrayList);
        System.out.println("Отсортированный ArrayList: ");
        System.out.println(arrayList);
    }

    /**
     * Метод сортирующий выбором массив
     *
     * @param array массив целых чисел
     */
    private static void selectionSort(int[] array) {
        for (int i = 0; i < array.length - 1; i++) {
            int min = array[i];
            int minIndex = i;
            for (int j = i + 1; j < array.length; j++) {
                if (array[j] < min) {
                    min = array[j];
                    minIndex = j;
                }
            }
            int replace = array[i];
            array[i] = array[minIndex];
            array[minIndex] = replace;
        }
    }

    /**
     * Метод сортирующий выбором ArrayList
     *
     * @param arrayList строчный массив целых чисел
     */
    private static void selectionSort(ArrayList<Integer> arrayList) {
        for (int i = 0; i < arrayList.size() - 1; i++) {
            int min = arrayList.get(i);
            int minIndex = i;
            for (int j = i + 1; j < arrayList.size(); j++) {
                if (arrayList.get(j) < min) {
                    min = arrayList.get(j);
                    minIndex = j;
                }
            }
            int replace = arrayList.get(i);
            arrayList.set(i, arrayList.get(minIndex));
            arrayList.set(minIndex, replace);
        }
    }
}
