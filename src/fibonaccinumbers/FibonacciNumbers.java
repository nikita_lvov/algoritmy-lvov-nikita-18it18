package fibonaccinumbers;

import java.util.Scanner;

/**
 * Класс нахождения n-го числа фибоначчи
 *
 * @author Nikita Lvov 18it18
 */
public class FibonacciNumbers {
    private static Scanner sc = new Scanner(System.in);

    public static void main(String[] args) {
        System.out.println("Введите число фибоначчи: ");
        int n = sc.nextInt();
        if (n < 0) {
            System.out.println("Факториал должен быть положительным числом");
            return;
        }
        System.out.println("Число фибоначчи, найденное итеративным способом : " + searchNumberFibonacci(n));
        System.out.println("Число фибоначчи, найденное рекурсивным способом : " + fibonacci(n));
    }

    /**
     * Метод нахождения n-го числа фибоначчи итеративным способом
     *
     * @param n число заданное с консоли
     * @return n число фибоначчи
     */
    private static int searchNumberFibonacci(int n) {
        int F1 = 1;
        int F2 = 1;
        for (int i = 3; i <= n; ++i) {
            int nextN = F1 + F2;
            F1 = F2;
            F2 = nextN;
        }
        return F2;
    }

    /**
     * Метод нахождения n-го числа фибоначчи рекурсивным способом
     *
     * @param n число заданное с консоли
     * @return n число фибоначчи
     */
    private static int fibonacci(int n) {
        if (n == 0) {
            return 0;
        } else if (n == 1) {
            return 1;
        } else {
            return fibonacci(n - 1) + fibonacci(n - 2);
        }
    }

    /**
     * Метод нахождения продолжительности выполнения операции
     */
    private static void time() {
        long before = System.currentTimeMillis();
//тут что-то происходит, продолжительность которого хотим померить
        long after = System.currentTimeMillis();
        System.out.println(after - before + " миллисекунд");
    }
}

